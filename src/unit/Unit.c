/* Unit.c generated by valac 0.36.6, the Vala compiler
 * generated from Unit.vala, do not modify */

/* Copyright 2016 Software Freedom Conservancy Inc.
 *
 * This software is licensed under the GNU Lesser General Public License
 * (version 2.1 or later).  See the COPYING file in this distribution.
 */

#include <glib.h>
#include <glib-object.h>




void unit_init (GError** error);
void unit_terminate (void);


void unit_init (GError** error) {
}


void unit_terminate (void) {
}



