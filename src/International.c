/* International.c generated by valac 0.36.6, the Vala compiler
 * generated from International.vala, do not modify */

/* Copyright 2016 Software Freedom Conservancy Inc.
 *
 * This software is licensed under the GNU LGPL (version 2.1 or later).
 * See the COPYING file in this distribution.
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>

#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))



#define TRANSLATABLE "translatable"
#define INTERNATIONAL_SUPPORT_SYSTEM_LOCALE ""
#define INTERNATIONAL_SUPPORT_LANGUAGE_SUPPORT_DIRECTORY _LANG_SUPPORT_DIR
void international_support_init (const gchar* package_name, gchar** args, int args_length1, const gchar* locale);
gchar* international_support_get_langpack_dir_path (gchar** args, int args_length1);


void international_support_init (const gchar* package_name, gchar** args, int args_length1, const gchar* locale) {
	const gchar* _tmp0_;
	const gchar* _tmp1_;
	gchar** _tmp2_;
	gint _tmp2__length1;
	gchar* _tmp3_;
	gchar* _tmp4_;
	const gchar* _tmp5_;
	const gchar* _tmp6_;
#line 15 "/home/jens/Source/shotwell/src/International.vala"
	g_return_if_fail (package_name != NULL);
#line 15 "/home/jens/Source/shotwell/src/International.vala"
	g_return_if_fail (locale != NULL);
#line 16 "/home/jens/Source/shotwell/src/International.vala"
	_tmp0_ = locale;
#line 16 "/home/jens/Source/shotwell/src/International.vala"
	setlocale (LC_ALL, _tmp0_);
#line 18 "/home/jens/Source/shotwell/src/International.vala"
	_tmp1_ = package_name;
#line 18 "/home/jens/Source/shotwell/src/International.vala"
	_tmp2_ = args;
#line 18 "/home/jens/Source/shotwell/src/International.vala"
	_tmp2__length1 = args_length1;
#line 18 "/home/jens/Source/shotwell/src/International.vala"
	_tmp3_ = international_support_get_langpack_dir_path (_tmp2_, _tmp2__length1);
#line 18 "/home/jens/Source/shotwell/src/International.vala"
	_tmp4_ = _tmp3_;
#line 18 "/home/jens/Source/shotwell/src/International.vala"
	bindtextdomain (_tmp1_, _tmp4_);
#line 18 "/home/jens/Source/shotwell/src/International.vala"
	_g_free0 (_tmp4_);
#line 19 "/home/jens/Source/shotwell/src/International.vala"
	_tmp5_ = package_name;
#line 19 "/home/jens/Source/shotwell/src/International.vala"
	bind_textdomain_codeset (_tmp5_, "UTF-8");
#line 20 "/home/jens/Source/shotwell/src/International.vala"
	_tmp6_ = package_name;
#line 20 "/home/jens/Source/shotwell/src/International.vala"
	textdomain (_tmp6_);
#line 70 "International.c"
}


gchar* international_support_get_langpack_dir_path (gchar** args, int args_length1) {
	gchar* result = NULL;
	GFile* local_langpack_dir = NULL;
	gchar** _tmp0_;
	gint _tmp0__length1;
	const gchar* _tmp1_;
	gchar* _tmp2_;
	gchar* _tmp3_;
	GFile* _tmp4_;
	GFile* _tmp5_;
	GFile* _tmp6_;
	GFile* _tmp7_;
	GFile* _tmp8_;
	GFile* _tmp9_;
	gchar* _tmp10_ = NULL;
	GFile* _tmp11_;
	gboolean _tmp12_;
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp0_ = args;
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp0__length1 = args_length1;
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp1_ = _tmp0_[0];
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp2_ = g_find_program_in_path (_tmp1_);
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp3_ = _tmp2_;
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp4_ = g_file_new_for_path (_tmp3_);
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp5_ = _tmp4_;
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp6_ = g_file_get_parent (_tmp5_);
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp7_ = _tmp6_;
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp8_ = g_file_get_child (_tmp7_, "locale-langpack");
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_tmp9_ = _tmp8_;
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_g_object_unref0 (_tmp7_);
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_g_object_unref0 (_tmp5_);
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	_g_free0 (_tmp3_);
#line 24 "/home/jens/Source/shotwell/src/International.vala"
	local_langpack_dir = _tmp9_;
#line 28 "/home/jens/Source/shotwell/src/International.vala"
	_tmp11_ = local_langpack_dir;
#line 28 "/home/jens/Source/shotwell/src/International.vala"
	_tmp12_ = g_file_query_exists (_tmp11_, NULL);
#line 28 "/home/jens/Source/shotwell/src/International.vala"
	if (_tmp12_) {
#line 127 "International.c"
		GFile* _tmp13_;
		gchar* _tmp14_;
#line 28 "/home/jens/Source/shotwell/src/International.vala"
		_tmp13_ = local_langpack_dir;
#line 28 "/home/jens/Source/shotwell/src/International.vala"
		_tmp14_ = g_file_get_path (_tmp13_);
#line 28 "/home/jens/Source/shotwell/src/International.vala"
		_g_free0 (_tmp10_);
#line 28 "/home/jens/Source/shotwell/src/International.vala"
		_tmp10_ = _tmp14_;
#line 138 "International.c"
	} else {
		gchar* _tmp15_;
#line 29 "/home/jens/Source/shotwell/src/International.vala"
		_tmp15_ = g_strdup (INTERNATIONAL_SUPPORT_LANGUAGE_SUPPORT_DIRECTORY);
#line 29 "/home/jens/Source/shotwell/src/International.vala"
		_g_free0 (_tmp10_);
#line 29 "/home/jens/Source/shotwell/src/International.vala"
		_tmp10_ = _tmp15_;
#line 147 "International.c"
	}
#line 28 "/home/jens/Source/shotwell/src/International.vala"
	result = _tmp10_;
#line 28 "/home/jens/Source/shotwell/src/International.vala"
	_g_object_unref0 (local_langpack_dir);
#line 28 "/home/jens/Source/shotwell/src/International.vala"
	return result;
#line 155 "International.c"
}



