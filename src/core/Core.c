/* Core.c generated by valac 0.36.6, the Vala compiler
 * generated from Core.vala, do not modify */

/* Copyright 2016 Software Freedom Conservancy Inc.
 *
 * This software is licensed under the GNU Lesser General Public License
 * (version 2.1 or later).  See the COPYING file in this distribution.
 */
/* This file is the master unit file for the Core unit.  It should be edited to include
 * whatever code is deemed necessary.
 *
 * The init() and terminate() methods are mandatory.
 *
 * If the unit needs to be configured prior to initialization, add the proper parameters to
 * the preconfigure() method, implement it, and ensure in init() that it's been called.
 */

#include <glib.h>
#include <glib-object.h>




void core_preconfigure (void);
void core_init (GError** error);
void core_terminate (void);


void core_preconfigure (void) {
}


void core_init (GError** error) {
}


void core_terminate (void) {
}



