/* _CameraInternals.c generated by valac 0.36.6, the Vala compiler
 * generated from _CameraInternals.vala, do not modify */

/* Copyright 2016 Software Freedom Conservancy Inc.
 *
 * This software is licensed under the GNU Lesser General Public License
 * (version 2.1 or later).  See the COPYING file in this distribution.
 *
 * Auto-generated file.  Do not modify!
 */

#include <glib.h>
#include <glib-object.h>



extern gint camera__unit_init_count;
gint camera__unit_init_count = 0;

void camera_init_entry (GError** error);
void unit_init_entry (GError** error);
void sidebar_init_entry (GError** error);
void camera_init (GError** error);
void camera_terminate_entry (void);
void camera_terminate (void);
void unit_terminate_entry (void);
void sidebar_terminate_entry (void);


void camera_init_entry (GError** error) {
	gint _tmp0_;
	GError * _inner_error_ = NULL;
#line 14 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	_tmp0_ = camera__unit_init_count;
#line 14 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	camera__unit_init_count = _tmp0_ + 1;
#line 14 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	if (_tmp0_ != 0) {
#line 15 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		return;
#line 42 "_CameraInternals.c"
	}
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	unit_init_entry (&_inner_error_);
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		g_propagate_error (error, _inner_error_);
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		return;
#line 52 "_CameraInternals.c"
	}
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	sidebar_init_entry (&_inner_error_);
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		g_propagate_error (error, _inner_error_);
#line 17 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		return;
#line 62 "_CameraInternals.c"
	}
#line 19 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	camera_init (&_inner_error_);
#line 19 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 19 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		g_propagate_error (error, _inner_error_);
#line 19 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		return;
#line 72 "_CameraInternals.c"
	}
}


void camera_terminate_entry (void) {
	gboolean _tmp0_ = FALSE;
	gint _tmp1_;
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	_tmp1_ = camera__unit_init_count;
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	if (_tmp1_ == 0) {
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		_tmp0_ = TRUE;
#line 86 "_CameraInternals.c"
	} else {
		gint _tmp2_;
		gint _tmp3_;
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		_tmp2_ = camera__unit_init_count;
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		camera__unit_init_count = _tmp2_ - 1;
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		_tmp3_ = camera__unit_init_count;
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		_tmp0_ = _tmp3_ != 0;
#line 98 "_CameraInternals.c"
	}
#line 23 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	if (_tmp0_) {
#line 24 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
		return;
#line 104 "_CameraInternals.c"
	}
#line 26 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	camera_terminate ();
#line 28 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	unit_terminate_entry ();
#line 28 "/home/jens/Source/shotwell/src/.unitize/_CameraInternals.vala"
	sidebar_terminate_entry ();
#line 112 "_CameraInternals.c"
}



