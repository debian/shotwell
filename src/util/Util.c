/* Util.c generated by valac 0.36.6, the Vala compiler
 * generated from Util.vala, do not modify */

/* Copyright 2016 Software Freedom Conservancy Inc.
 *
 * This software is licensed under the GNU Lesser General Public License
 * (version 2.1 or later).  See the COPYING file in this distribution.
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>




#define UTIL_FILE_ATTRIBUTES "standard::*,time::*,id::file,id::filesystem,etag::value"
void util_init (GError** error);
void util_terminate (void);


void util_init (GError** error) {
}


void util_terminate (void) {
}



