/* FSpotTableBehavior.c generated by valac 0.36.6, the Vala compiler
 * generated from FSpotTableBehavior.vala, do not modify */

/* Copyright 2016 Software Freedom Conservancy Inc.
 *
 * This software is licensed under the GNU Lesser General Public License
 * (version 2.1 or later).  See the COPYING file in this distribution.
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>


#define DATA_IMPORTS_FSPOT_DB_TYPE_FSPOT_TABLE_BEHAVIOR (data_imports_fspot_db_fspot_table_behavior_get_type ())
#define DATA_IMPORTS_FSPOT_DB_FSPOT_TABLE_BEHAVIOR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), DATA_IMPORTS_FSPOT_DB_TYPE_FSPOT_TABLE_BEHAVIOR, DataImportsFSpotDbFSpotTableBehavior))
#define DATA_IMPORTS_FSPOT_DB_IS_FSPOT_TABLE_BEHAVIOR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DATA_IMPORTS_FSPOT_DB_TYPE_FSPOT_TABLE_BEHAVIOR))
#define DATA_IMPORTS_FSPOT_DB_FSPOT_TABLE_BEHAVIOR_GET_INTERFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), DATA_IMPORTS_FSPOT_DB_TYPE_FSPOT_TABLE_BEHAVIOR, DataImportsFSpotDbFSpotTableBehaviorIface))

typedef struct _DataImportsFSpotDbFSpotTableBehavior DataImportsFSpotDbFSpotTableBehavior;
typedef struct _DataImportsFSpotDbFSpotTableBehaviorIface DataImportsFSpotDbFSpotTableBehaviorIface;

struct _DataImportsFSpotDbFSpotTableBehaviorIface {
	GTypeInterface parent_iface;
	gchar* (*get_table_name) (DataImportsFSpotDbFSpotTableBehavior* self);
	gchar** (*list_columns) (DataImportsFSpotDbFSpotTableBehavior* self, int* result_length1);
	void (*build_row) (DataImportsFSpotDbFSpotTableBehavior* self, sqlite3_stmt* stmt, gpointer* row, gint offset);
};



GType data_imports_fspot_db_fspot_table_behavior_get_type (void) G_GNUC_CONST;
gchar* data_imports_fspot_db_fspot_table_behavior_get_table_name (DataImportsFSpotDbFSpotTableBehavior* self);
gchar** data_imports_fspot_db_fspot_table_behavior_list_columns (DataImportsFSpotDbFSpotTableBehavior* self, int* result_length1);
void data_imports_fspot_db_fspot_table_behavior_build_row (DataImportsFSpotDbFSpotTableBehavior* self, sqlite3_stmt* stmt, gpointer* row, gint offset);


gchar* data_imports_fspot_db_fspot_table_behavior_get_table_name (DataImportsFSpotDbFSpotTableBehavior* self) {
#line 20 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	g_return_val_if_fail (DATA_IMPORTS_FSPOT_DB_IS_FSPOT_TABLE_BEHAVIOR (self), NULL);
#line 20 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	return DATA_IMPORTS_FSPOT_DB_FSPOT_TABLE_BEHAVIOR_GET_INTERFACE (self)->get_table_name (self);
#line 46 "FSpotTableBehavior.c"
}


gchar** data_imports_fspot_db_fspot_table_behavior_list_columns (DataImportsFSpotDbFSpotTableBehavior* self, int* result_length1) {
#line 22 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	g_return_val_if_fail (DATA_IMPORTS_FSPOT_DB_IS_FSPOT_TABLE_BEHAVIOR (self), NULL);
#line 22 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	return DATA_IMPORTS_FSPOT_DB_FSPOT_TABLE_BEHAVIOR_GET_INTERFACE (self)->list_columns (self, result_length1);
#line 55 "FSpotTableBehavior.c"
}


void data_imports_fspot_db_fspot_table_behavior_build_row (DataImportsFSpotDbFSpotTableBehavior* self, sqlite3_stmt* stmt, gpointer* row, gint offset) {
#line 24 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	g_return_if_fail (DATA_IMPORTS_FSPOT_DB_IS_FSPOT_TABLE_BEHAVIOR (self));
#line 24 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	DATA_IMPORTS_FSPOT_DB_FSPOT_TABLE_BEHAVIOR_GET_INTERFACE (self)->build_row (self, stmt, row, offset);
#line 64 "FSpotTableBehavior.c"
}


static void data_imports_fspot_db_fspot_table_behavior_base_init (DataImportsFSpotDbFSpotTableBehaviorIface * iface) {
#line 19 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	static gboolean initialized = FALSE;
#line 19 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
	if (!initialized) {
#line 19 "/home/jens/Source/shotwell/plugins/shotwell-data-imports/FSpotTableBehavior.vala"
		initialized = TRUE;
#line 75 "FSpotTableBehavior.c"
	}
}


/**
 * This class defines a generic table behavior. In practice, it implements
 * the concept of a DAO (Data Access Object) in ORM terms and is responsible
 * for transforming the data extracted from a relational statement into a
 * lightweight value object.
 *
 * The type T defined in the generic is the value object type a behavior
 * implementation is designed to handle. Value object types are designed to
 * contain the data for a single database row.
 */
GType data_imports_fspot_db_fspot_table_behavior_get_type (void) {
	static volatile gsize data_imports_fspot_db_fspot_table_behavior_type_id__volatile = 0;
	if (g_once_init_enter (&data_imports_fspot_db_fspot_table_behavior_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (DataImportsFSpotDbFSpotTableBehaviorIface), (GBaseInitFunc) data_imports_fspot_db_fspot_table_behavior_base_init, (GBaseFinalizeFunc) NULL, (GClassInitFunc) NULL, (GClassFinalizeFunc) NULL, NULL, 0, 0, (GInstanceInitFunc) NULL, NULL };
		GType data_imports_fspot_db_fspot_table_behavior_type_id;
		data_imports_fspot_db_fspot_table_behavior_type_id = g_type_register_static (G_TYPE_INTERFACE, "DataImportsFSpotDbFSpotTableBehavior", &g_define_type_info, 0);
		g_type_interface_add_prerequisite (data_imports_fspot_db_fspot_table_behavior_type_id, G_TYPE_OBJECT);
		g_once_init_leave (&data_imports_fspot_db_fspot_table_behavior_type_id__volatile, data_imports_fspot_db_fspot_table_behavior_type_id);
	}
	return data_imports_fspot_db_fspot_table_behavior_type_id__volatile;
}



