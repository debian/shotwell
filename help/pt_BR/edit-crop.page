<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="edit-crop" xml:lang="pt-BR">

    <info>
        <link type="guide" xref="index#edit"/>
        <desc>Melhore a composição de uma foto cortando partes dela.</desc>
        
        <link type="next" xref="edit-external"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felipe Braga</mal:name>
      <mal:email>fbobraga@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

   <title>Cortando</title>

    <p>Para reduzir a área de uma foto e concentrar a atenção do espectador em uma parte menor dela, use a ferramenta de cortar. A ferramenta de cortar estará disponível apenas nos modos de janela toda ou tela cheia.</p>

<steps>
 <item>
  <p>Clique duas vezes em uma foto para entrar no modo janela toda e use o botão <gui>Cortar</gui> na barra de ferramentas.</p>
 </item>
 <item>
  <p>Um retângulo branco, o "retângulo de corte", aparecerá sobre a foto. A parte mais clara da foto dentro do retângulo de corte representa o que a foto parecerá quando você cortá-la.</p>
 </item>
 <item>
  <p>Arraste o retângulo para posicioná-lo e ajuste seu tamanho, arrastando suas bordas. Quando você move ou ajustar o tamanho do retângulo de corte, aparecem quatro linhas dentro dele, como uma grade de jogo da velha: são as linhas da <em>regra dos terços</em>.</p>
  <p>Você também pode restringir o retângulo de corte para um de muitos tamanhos comuns. Escolha um tamanho a partir da lista que se adapte às suas necessidades. Se você pressionar o botão ao lado da lista, a orientação da restrição irá mudar (de paisagem para retrato, e vice-versa).</p>
 </item>
 <item>
  <p>Após acertar o seu retângulo de corte, aplica a alteração pressionando o botão <gui>Cortar</gui>. O Shotwell irá mostrar a foto cortada.</p>
 </item>
 <item>
  <p>Se você mudar de ideia, pressione o botão <gui>Cortar</gui> novamente e ajuste o corte.</p>
  <p>Se você pressionar <gui>Cancelar</gui> ao invés de <gui>Aplicar</gui>, o Shotwell voltará às dimensões anteriores da foto.</p>
 </item>
</steps>

<section id="rule-of-thirds">
 <title>O que é a regra dos terços?</title>
 <p>A <em>regra dos terços</em> ajuda você a escolher uma composição agradável para uma foto.</p>
 <p>Imagine que a cena é dividida em uma grade de 3x3 por duas linhas verticais igualmente espaçadas e duas linhas horizontais igualmente espaçadas. De acordo com a regra, você terá mais chances de obter uma composição agradável se você alinhar as características principais (como o horizonte, ou corpo de uma pessoa) com uma das linhas. Prestar atenção ao caminho que o fluxo de recursos faz de uma parte da grade para outra também pode ajudar.</p>
 <p>Cortar uma foto de acordo com a regra dos terços geralmente resulta em uma imagem mais atraente.</p>
 <media type="image" src="figures/crop_thirds.jpg">
  <p>Recortar uma foto, usando a "regra dos terços" para melhorar a composição.</p>
 </media>
</section>
</page>
