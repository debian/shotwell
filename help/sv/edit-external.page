<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="edit-external" xml:lang="sv">

    <info>
        <link type="guide" xref="index#edit"/>
        <desc>Använda ett annat program för att redigera en bild.</desc>
        
        <link type="next" xref="edit-redeye"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Josef Andersson</mal:name>
      <mal:email>josef.andersson@fripost.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

    <title>Redigera bilder med ett externt program</title>

    <p>Du kan vilja använda en extern redigerare för att utföra ytterligare arbete på en bild. Om installerade så är GIMP och UFRaw de externa standardredigerarna för bilder och RAW-redigering. Om dessa program inte är installerade måste du välja dina föredragna redigerare genom att välja <guiseq><gui>Redigera</gui><gui>Inställningar</gui></guiseq> och markera redigerare från rullgardinsmenyn för installerade program.</p>

    <p>När redigerare väl har angetts, markera en bild och väljs <guiseq><gui>Bilder</gui> <gui>Öppna med extern redigerare</gui></guiseq> för att öppna bilden med den externa redigeraren. På samma sätt, om originalbilden är en RAW-fil, välj <guiseq><gui>Bilder</gui><gui>Öppna med RAW-redigerare</gui></guiseq> för att redigera RAW-filen med den valda RAW-redigeraren.</p>

    <p>När du är klar med redigering och sparar filen kommer Shotwell att upptäcka ändringarna och uppdatera bilden. Om externa ändringar har skett, tryck och håll <key>Skift</key>-tangenten i helskärmsvyn för att visa originalbilden istället för den externt redigerade.</p>
                            
    <p>Återställning till originalet kommer att radera externa ändringar.</p>
    
    <note>
        <p>Om du externt redigerar en RAW-bild och sparar resultatet till en annan bild, som JPEG eller PNG, kan inte Shotwell automatiskt bestämma att den ursprungliga RAW-filen och den nya bilden ska kopplas samman.</p>
            
        <p>Om du vill arbeta med bildresultatet i Shotwell måste du importera det.</p>
    </note>    
</page>
