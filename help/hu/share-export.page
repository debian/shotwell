<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="share-export" xml:lang="hu">

    <info>
        <link type="guide" xref="index#share"/>
        <desc>Másolja ki fényképeit a Shotwellből, hogy máshol is használhassa azokat.</desc>
        
        <link type="next" xref="share-print"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>urbalazs at gmail dot hu</mal:email>
      <mal:years>2015, 2016.</mal:years>
    </mal:credit>
  </info>

	<title>Fényképek exportálása</title>

<p>A fényképek exportálásához fogd és vidd módszerrel húzza ki a fényképeket a Shotwellből egy fájlkezelő ablakba vagy az asztalra. Az új fájlok a gyűjteményében lévők teljes méretű másolatai lesznek.</p>

<p>Ennek alternatívájaként válassza ki fényképek egy csoportját, és válassza a <guiseq><gui>Fájl</gui><gui>Exportálás…</gui></guiseq> menüpontot, vagy nyomja meg a <keyseq><key>Shift</key><key>Ctrl</key><key>E</key></keyseq> kombinációt. Ezzel a fényképeket úgy exportálhatja, hogy közben a fájlok méretét és felbontását finomhangolhatja. A megjelenő ablakban több dolgot is kiválaszthat:</p>

<list>
<item><p>A formátumot az exportáláshoz.</p>
<list>
<item><p>Válassza a <gui>Módosítatlan</gui> lehetőséget az eredeti formátumban való exportáláshoz, a Shotwellben végzett módosítások nélkül. A RAW képek az eredeti RAW formátumban kerülnek exportálásra.</p></item>
<item><p>Válassza a <gui>Jelenlegi</gui> lehetőséget a fényképek exportálásához a Shotwellben végzett módosításokkal együtt. A RAW fényképek JPEG formátumban kerülnek exportálásra, ha szerkesztette azokat a Shotwellben, egyébként pedig az eredeti RAW formátumukban.</p></item>
<item><p>Kiválaszthat egy adott képformátumot (JPEG, PNG, TIFF, BMP) is az exportáláshoz való használatra. A Shotwellben végzett minden módosítást tartalmazni fog, és a Shotwell a fényképeket átalakítja a cél formátumra.</p></item>
</list>
</item>
<item><p>Az exportáláshoz használandó képminőség (alacsony, közepes, magas vagy maximális).</p></item>
<item><p>Méretezési megszorítás (azaz a Shotwell eldönti, hogy a fényképeket le kell-e méretezni), és a kívánt képpontméret.</p></item>
<item><p>The option whether you like to export metadatas such as tags or ratings. This can help to save your privacy if you have geolocation tags or tags which shouldn't be seen by anyone.</p></item>
</list>

<p>If selected, Shotwell will write tags, titles, and other metadata to the new files.</p>

</page>
