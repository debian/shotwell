<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="edit-nondestructive" xml:lang="hu">

    <info>
        <link type="guide" xref="index#edit"/>
        <desc>Shotwell is a non-destructive photo editor - it does not modify your original photos.</desc>
        
        <link type="next" xref="edit-date-time"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>urbalazs at gmail dot hu</mal:email>
      <mal:years>2015, 2016.</mal:years>
    </mal:credit>
  </info>

	<title>What happens to the original when I edit a photo?</title>

<p>Shotwell is a non-destructive photo editor. It does not modify your original photographs. That is to say, if you crop a photo or adjust its colors, the photo file on disc remains untouched. Shotwell stores your edits in a database and applies them on the fly as necessary. This means you can undo any alterations you make to a photograph.</p>

<p>If you want to see what a photo looked like before your modifications, press the <key>Shift</key> key. The original photo will be displayed as long as you hold the key down.</p>

<p>Note that Shotwell can optionally write metadata (such as tags and titles) to photo files.
For more information, see the section <link xref="other-files">Photo files</link>.</p>

</page>
