<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="edit-rotate" xml:lang="es">

    <info>
        <link type="guide" xref="index#edit"/>
        <desc>Pulse el botón <gui>Girar</gui>, o elija uno de los comandos en el menú <gui>Foto</gui>.</desc>
        
        <link type="next" xref="edit-straighten"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marina Prado</mal:name>
      <mal:email>mapraro93@hotmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2014 - 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dámaris Letelier</mal:name>
      <mal:email>dam.letelier@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

	<title>Girar o voltear una foto</title>

    <p>Puede girar las fotos a la izquierda y a la derecha con el botón <gui>Girar</gui> en la barra de herramientas de la mayoría de las vistas. También puede hacer una imagen reflejada de cualquier foto.</p>
    
    <p>Para girar hacia la derecha, pulse el botón <gui>Girar</gui>. Para girar hacia la izquierda, mantenga pulsada la tecla <key>Ctrl</key> y luego pulse el botón. Ambos comandos están disponibles en el menú <gui>Fotos</gui>. Como alternativa, utilice los siguientes atajos del teclado:</p>
    <list>
    <item><p>rotar a la izquierda: <keyseq><key>Mayús</key><key>Ctrl</key><key>R</key></keyseq> o <key>[</key></p></item>
    <item><p>rotar a la derecha: <keyseq><key>Ctrl</key><key>R</key></keyseq> or <key>]</key></p></item>
    </list>
    <p>Para crear una imagen reflejada de una foto, utilice el comando <gui>Voltear horizontalmente</gui> en el menú <gui>Fotos</gui>. Para voltear una imagen verticalmente, utilice el comando <gui>Voltear verticalmente</gui> en el mismo menú.</p>
    
    <note>
     <p>Si selecciona más de una imagen, puede girar todas ellas al mismo tiempo.</p>
    </note>
    
</page>
