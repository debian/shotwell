<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="rating" xml:lang="es">

    <info>
        <link type="guide" xref="index#organize"/>
        <desc>Asigne a las fotos una puntuación entre 1 y 5. Puede rechazar las fotos malas, ocultándolas en la vista.</desc>
        
        <link type="next" xref="delete"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marina Prado</mal:name>
      <mal:email>mapraro93@hotmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2014 - 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dámaris Letelier</mal:name>
      <mal:email>dam.letelier@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

	<title>Valoraciones</title>
<p>Puede asignar a cada foto una puntuación entre 1 y 5, o marcarla como rechazada, en cuyo caso Shotwell la ocultará de manera predeterminada.</p>

<p>Puede valorar una foto o un conjunto de fotos de cualquiera de estas maneras:</p>

<list>
<item><p>Seleccione las fotos y luego escoja una puntuación en el menú <guiseq><gui>Foto</gui><gui>Establecer puntuación</gui></guiseq> del nivel superior.</p></item>
<item><p>Pulse con el botón derecho en las fotos y luego escoja su puntuación en el menú contextual «Establecer puntuación».</p></item>
<item><p>Seleccione la foto o las fotos, luego pulse cualquiera de los atajos del teclado <key>1</key>, <key>2</key>, <key>3</key>, <key>4</key> o <key>5</key> para asignar una puntuación. Pulse <key>9</key> para marcar las fotos como rechazadas o pulse <key>0</key> para borrar las puntuaciones.</p></item>
</list>

<p>Normalmente, Shotwell muestra todas las fotos excepto las rechazadas. Puede establecer un filtro de puntuaciones diferente utilizando el menú <guiseq><gui>Vista</gui><gui>Filtrar fotos</gui></guiseq>. Por ejemplo, puede mostrar sólo las fotos con puntuación de 3 estrellas o más, o puede mostrar todas las fotos, incluidas las marcadas como rechazadas. El icono Shotwell de la barra de herramientas muestra el filtro de puntuación actual y también se puede utilizar para configurar el filtro.</p>

<p>Shotwell normalmente muestra la puntuación de cada foto en su esquina inferior izquierda. Puede desactivar la visualización de las puntuaciones utilzando el elemento del menú <guiseq><gui>Vista</gui><gui>Valoraciones</gui></guiseq>.</p>

<p>Puede aumentar o disminuir la puntuación de una foto utilizando los comandos <guiseq><gui>Establecer puntuación</gui><gui>Aumentar</gui></guiseq> y <guiseq><gui>Establecer puntuación</gui><gui>Disminuir</gui></guiseq>, o los atajos del teclado <key>&lt;</key> y <key>&gt;</key>.</p>
</page>
