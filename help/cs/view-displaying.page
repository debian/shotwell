<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="view-view" xml:lang="cs">

    <info>
        <link type="guide" xref="index#view"/>
        <desc>Zjistěte si, jakými různými způsoby si můžete své fotografie zobrazit: v mřížce, přes celé hlavní okno nebo přes celou obrazovku.</desc>
        
        <link type="next" xref="view-sidebar"/>
    </info>

	<title>Způsoby zobrazení fotografií</title>

<p>Když v postranním panelu vyberete sbírku, Shotwell zobrazí všechny její fotografie v hlavní části okna. V pravém dolním rohu je posuvník, kterým si můžete upravit velikost náhledů. Případně k tomu můžete použít klávesy plus a mínus (<key>+</key> a <key>-</key>) nebo držet zmáčknutý <key>Ctrl</key> a použít kolečko myši.</p>

<p>Dvojitým kliknutím na fotografii si ji zobrazíte v plné velikosti. Na další fotografie ve sbírce pak můžete přecházet pomocí tlačítek Zpět a Vpřed. Pro návrat do sbírky na fotografii opět dvojitě klikněte nebo zmáčkněte <key>Esc</key>.</p>

<p>Když prohlížíte fotografii v režimu celého okna, posuvník na nástrojové liště řídí přiblížení. Fotografii můžete posouvat jejím chycením v kterékoliv části a tažením. Přibližovat můžete také pomocí kolečka myši nebo pomocí následujících klávesových zkratek: <keyseq><key>Ctrl</key>0</keyseq> pro obrázek v plné velikosti, <keyseq><key>Ctrl</key>1</keyseq> pro 100% (1 pixel fotografie = 1 pixel obrazovky) a <keyseq><key>Ctrl</key>2</keyseq> pro 200% (1 pixel obrazovky = 2×2 pixely obrazovky).</p>

<p>Shotwell také nabízí k prohlížení fotografií režim celé obrazovky. Zvolte <guiseq><gui>Zobrazit</gui> <gui>Celá obrazovka</gui></guiseq> nebo zmáčkněte <key>F11</key>. Abyste v režimu celé obrazovky viděli nástrojovou lištu, musíte najet myší do spodní části obrazovky. Nástrojová lišta nabízí tlačítka pro pohyb ve sbírce, pro přišpendlení nástrojové lišty (aby se neskrývala, když myší odjedete) a k opuštění režimu celé obrazovky.</p>

<section id="viewing-videos">
<title>Zobrazování videí</title>
<p>Když dvojitě kliknete na video, Shotwell spustí externí videopřehrávač, aby video přehrál. V současnosti není možné přehrát video v režimu celé obrazovky aplikace Shotwell nebo jinak přímo v rámci Shotwellu.</p>
</section>
    
</page>
