<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="edit-crop" xml:lang="cs">

    <info>
        <link type="guide" xref="index#edit"/>
        <desc>Jak zdokonalit kompozici fotografie odříznutím některých částí.</desc>
        
        <link type="next" xref="edit-external"/>
    </info>

   <title>Ořezání</title>

    <p>Jestli chcete zmenšit plochu fotografie a zaměřit pohled jen na nějakou její část, použijte nástroj pro ořezání. Tento nástroj je přístupný jen v režimu celého okna nebo celé obrazovky.</p>

<steps>
 <item>
  <p>Dvojitým kliknutím na fotografii se přepněte do režimu celého okna a po té zmáčkněte na nástrojové liště tlačítko <gui>Oříznout</gui>.</p>
 </item>
 <item>
  <p>Přes fotografii se objeví ořezové hranice v podobě bílého obdélníku. Světlejší část uvnitř ořezového obdélníku odpovídá tomu, co z fotografie uvidíte po jejím ořezání.</p>
 </item>
 <item>
  <p>Když umístíte ukazatel do vnitřní části obdélníku, můžete obdélníkem posouvat. Přetahováním hran obdélníku můžete přizpůsobovat jeho velikost. Když obdélník přesouváte nebo měníte jeho velikost, uvidíte uvnitř něj mřížku. Ta odpovídá čárám <em>pravidla třetin</em>.</p>
  <p>Obdélník můžete také upravit na některou z běžných velikostí. V rozbalovacím seznamu si zvolte velikost, kterou potřebujete. Zmáčknutím otáčecího tlačítka vedle tohoto výběru můžete změnit orientaci ořezu (z krajiny na portrét).</p>
 </item>
 <item>
  <p>Až jste s výběrem ořezávané části spokojeni, zmáčkněte tlačítko <gui>Oříznout</gui>. Shotwell zobrazí ořezanou fotografii.</p>
 </item>
 <item>
  <p>Pokud změníte ohledně ořezu názor, zmáčkněte tlačítko <gui>Oříznout</gui> znovu a ořez si dolaďte.</p>
  <p>Při zmáčknutí <gui>Zrušit</gui> místo <gui>Použít</gui> vrátí Shotwell fotografie na původní ořezané rozměry.</p>
 </item>
</steps>

<section id="rule-of-thirds">
 <title>Co je to pravidlo třetin?</title>
 <p><em>Pravidlo třetin</em> pomáhá zvolit uspokojivou kompozici fotografie.</p>
 <p>Pomyslně si scénu rozdělte do mřížky 3×3 pomocí dvou rovnoměrně rozmístěných svislých a dvou rovnoměrně rozmístěných vodorovných čar. Při dodržení tohoto pravidla spíše získáte uspokojivou kompozici, když hlavní objekty zájmu (jako je horizont nebo tělo osoby) umístíte k těmto čarám. Povšimněte si také možnosti přesunu jedné části mřížky do jiné, což může rovněž pomoci.</p>
 <p>Oříznutí fotografie podle pravidla třetin čas přinese výsledek v podobě mnohem působivějšího obrázku.</p>
 <media type="image" src="figures/crop_thirds.jpg">
  <p>Ořezáním fotografie za pomocí „pravidla třetin“ můžete vylepšit její kompozici.</p>
 </media>
</section>
</page>
